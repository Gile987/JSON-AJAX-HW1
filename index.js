// by Vince - primer 2
var sending = false;

// type = POST, url = "http://localhost/ime_projekta/list.php", data = {name: "upisano ime", email: "upisan email"}
function createRequest(type, url, data, callbacks) {
	var httpRequest = new XMLHttpRequest();
	httpRequest.open(type, url, true);
	httpRequest.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
	// data = "{"name":"upisano ime","email":"upisan email"}"
	httpRequest.send(JSON.stringify(data));

	for (var event in callbacks) {
		httpRequest.addEventListener(event, callbacks[event]);
	}
}

document.forms['signup'].addEventListener('submit', function (e) {
	e.preventDefault();
	
	if (sending) {
		return;
	}

	var form = this;

	//  data = {name: "upisano ime", email: "upisan email"} 
	var data = {
		name: form.elements['name'].value,
		email: form.elements['email'].value
	};

	sending = true;

	createRequest('POST', form.action, data, {
		loadend: function (e) {
			sending = false;
		},
		load: function (e) {
			// response = {status: 200, data: "User -upisano ime- successfully added to the mailing list"}
			var response = JSON.parse(this.responseText);
			var newLocal = response.data;
			
			alert(newLocal);
		}
	});
});