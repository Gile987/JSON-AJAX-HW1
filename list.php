<?php

function response($data = '', $status = 200) {
    // json_encode array('status' => $status, 'data' => $data) = {"status":$status,"data":$data}
    return json_encode(array(
        'status' => $status,
        'data' => $data
    ));
}

header('Content-Type: application/json; charset=utf-8');

/*
http://php.net/manual/en/wrappers.php.php
file_get_contents('php://input') chita podatke pre no shto su "stavljeni" u $_POST ili $_REQUEST superglobale. read-only
*/
$raw = file_get_contents('php://input');
/* 
http://php.net/manual/en/function.json-decode.php
This function only works with UTF-8 encoded strings!
When TRUE, returned objects will be converted into associative arrays. 
Trailing commas are not allowed!
*/
$data = json_decode($raw, TRUE);
// preuzmi ime i email iz $data ina kraj dodaj "\r\n" = carriage-return + new line (https://stackoverflow.com/questions/3091524/what-are-carriage-return-linefeed-and-form-feed)
$data2 = $data['name'] . '-' . $data['email'] . "\r\n";
/*
http://php.net/manual/en/function.flock.php
http://php.net/manual/en/function.file-put-contents.php
FILE_APPEND = ako fajl postoji, dodaj u njega umesto overwrite
LOCK_EX = sprechi nekoga drugoga da u isto vreme pishe u fajl (nepotrebno ovde, al' neka ga)
*/
$ret = file_put_contents('formdata.txt', $data2, FILE_APPEND | LOCK_EX);

echo response('User ' . $data['name'] . ' successfully added to the mailing list');

?>